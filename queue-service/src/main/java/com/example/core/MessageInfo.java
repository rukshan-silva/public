package com.example.core;

import java.io.Serializable;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import com.example.service.impl.FileQueueService;

/**
 * This class represents a Message object - a DTO which holds message details
 * 
 * @author Rukshan Silva, 12 Jun 2016
 * @version 1.0.0
 */
public class MessageInfo implements Serializable{

	/**
	 * Implements {@link Serializable} just in case this message object is used for {@link FileQueueService}
	 */
	private static final long serialVersionUID = 2733230116854454663L;

	// Unique message Id
	private String messageId;
	
	// Message body
	private String body;
	
	// Message queue url - which the message belongs to
	private String queueUrl;
	
	// Message timeout - the 'invisibility' property to see for how long the message should be disappeared - Defaulted to 5
	private Long messageTimeout =  5L;
	
	// Message timeout units - Defaulted to SECONDS
	private TimeUnit messageTimeUnit = TimeUnit.SECONDS;
	
	public MessageInfo(String body, String queueUrl) {
		
		this.messageId = UUID.randomUUID().toString();
		this.body = body;
		this.queueUrl = queueUrl;
	}

	/* Can override the default message timeout/ timeout unit for each message */
	public MessageInfo(String body, String queueUrl, Long messageTimeout, TimeUnit messageTimeUnit) {
		
		this(body, queueUrl);
		this.messageTimeout = messageTimeout;
		this.messageTimeUnit = messageTimeUnit;
	}
	
	/* Can override the default message id for each message */
	public MessageInfo(String messageId, String body, String queueUrl, Long messageTimeout, TimeUnit messageTimeUnit) {
		
		this(body, queueUrl, messageTimeout, messageTimeUnit);
		this.messageId = messageId;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (obj instanceof MessageInfo)
			return (this.messageId.equals(((MessageInfo) obj).getMessageId()) &&
				(this.queueUrl.equals(((MessageInfo) obj).getQueueUrl())) &&
					(this.body.equals(((MessageInfo) obj).getBody())));
		
		return false;
	}
	
	@Override
	public int hashCode() {

		Integer hash = 7;
		hash = hash  * 11 + messageId.hashCode();
		hash = hash  * 17 + queueUrl.hashCode();
		hash = hash  * 31 + body.hashCode();
		
		return hash.intValue();
	}
	
	@Override
	public String toString() {
		return ("message [id='"+ messageId + "', body='"+ body + "', queueUrl='"+ queueUrl + 
					"', messageTimeout='" + messageTimeout + "']");
	}
	
	/**
	 * @return the messageId
	 */
	public String getMessageId() {
		return messageId;
	}

	/**
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @return the queueUrl
	 */
	public String getQueueUrl() {
		return queueUrl;
	}

	/**
	 * @return the messageTimeout
	 */
	public Long getMessageTimeout() {
		return messageTimeout;
	}

	/**
	 * @return the timeUnit
	 */
	public TimeUnit getMessageTimeUnit() {
		return messageTimeUnit;
	}
}
