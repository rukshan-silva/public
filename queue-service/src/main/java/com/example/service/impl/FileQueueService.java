package com.example.service.impl;

import com.amazonaws.services.sqs.model.UnsupportedOperationException;
import com.example.core.MessageInfo;
import com.example.service.QueueService;

public class FileQueueService implements QueueService {
	
	//
	// Task 3: Implement me if you have time.
	//
	@Override
	public void push(MessageInfo message) {
		
		throw new UnsupportedOperationException("Not implemented, yet");
	}

	@Override
	public MessageInfo pull(String queueUrl) {

		throw new UnsupportedOperationException("Not implemented, yet");
	}

	@Override
	public void delete(String messageId) {

		throw new UnsupportedOperationException("Not implemented, yet");
	}

	@Override
	public void shutdown() {

		throw new UnsupportedOperationException("Not implemented, yet");
	}
}
