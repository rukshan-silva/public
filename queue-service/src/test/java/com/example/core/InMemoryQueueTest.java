package com.example.core;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.example.service.QueueService;
import com.example.service.impl.InMemoryQueueService;

/**
 * InMemoryQueue Test Class
 * 
 *  * I still believe this could be improved further. This is the only way I could think of *
 * 
 * @author Rukshan Silva, 12 Jun 2016
 * @modified 19 Jun 2016
 * @version 1.0.0
 */
public class InMemoryQueueTest {

	//Number of items to be added to the queue
	private static final Integer PUSH_ITEMS_SIZE = 10;

	/*
	 * Number of items to be retrieved from the queue
	 * This would be twice as the Number of items to be added as the items would be added again to the queue after the 
	 * timeout - 'invisible' period, and will be retrieving those again
	 * 
	 * If running only a single functionality (ie: pull or push but not both), need to change this accordingly
	 */
	private static final Integer PULL_ITEMS_SIZE = 2 * PUSH_ITEMS_SIZE;

	//Delete the pushed items from the queue - would be the same number as the pushed
	private static final Integer DELETE_ITEMS_SIZE = PUSH_ITEMS_SIZE;

	/*
	 * Have a countdown for the total of both pushed and pulled items. This is because to make sure both push and pull tasks
	 * should be completed to progress further
	 */
	private static final Integer PULL_COUNTDOWN_SIZE = PUSH_ITEMS_SIZE + PULL_ITEMS_SIZE;

	//Delete the pushed items from the queue - would be the same number as the pushed
	private static final Integer DELETE_COUNTDOWN_SIZE = PUSH_ITEMS_SIZE;
	
	private QueueService queueService;

	private QueueHandler queueHandler;

	private ExecutorService executorService;

	private List<String> messageList;
	
	/**
	 * Setting up - Initialize the objects
	 */
	@Before
	public void setup() {

		queueHandler = new QueueHandler(10, 10, 10);
		queueService = new InMemoryQueueService(10);
		queueHandler.setQueueService(queueService);

		messageList = new CopyOnWriteArrayList<>();
		executorService = Executors.newFixedThreadPool(10);
	}

	/**
	 * This tests the Queue functionality
	 * 
	 * Has integrated all push/ pull/ delete functionalities together into a single @Test
	 * But, calls individual methods for each functionality, which runs in parallel
	 */
	@Test
	public void testQueueMessages() {

		final CountDownLatch pullCountDown = new CountDownLatch(PULL_COUNTDOWN_SIZE);
		final CountDownLatch deleteCountDown = new CountDownLatch(DELETE_COUNTDOWN_SIZE);

		testQueuePush(pullCountDown);

		testQueuePull(pullCountDown);

		testQueueDelete(pullCountDown, deleteCountDown);

		//Wait until all the functionalities are completed before exiting the program
		try {
			deleteCountDown.await(); // main thread is waiting on CountDownLatch to finish
		} catch (InterruptedException ie) {
			System.err.println("Error - " + ie.getMessage());
			ie.printStackTrace();
		}
	}

	/**
	 * This tests the Push functionality 
	 */
	private void testQueuePush(CountDownLatch pullCountDown) {

		System.out.println("===== starting QueuePush test ========");

		for (int i = 0; i < PUSH_ITEMS_SIZE; i++) {

			executorService.submit(() -> {

				try {
					String messageId = UUID.randomUUID().toString();
					messageList.add(messageId);
					queueHandler.push(new MessageInfo(messageId, "Test Message Body ", "test-timeout-queue", 25L, TimeUnit.MILLISECONDS));

				} catch (IllegalArgumentException e) {
					System.err.println("Error - " + e.getMessage());
					e.printStackTrace();
				} finally {
					pullCountDown.countDown(); // reduce count of CountDownLatch by 1
				}
			});
		}
	}
	
	/**
	 * This tests the Pull/ Timeout functionality 
	 */
	private void testQueuePull(CountDownLatch pullCountDown) {

		System.out.println("===== starting QueuePull test ========");

		for (int i = 0; i < PULL_ITEMS_SIZE; i++) {

			executorService.submit(() -> {

				try {

					MessageInfo message = queueHandler.pull("test-timeout-queue");
					
					Assert.assertTrue(messageList.contains(message.getMessageId()));
				} catch (IllegalArgumentException | AssertionError e) {
					System.err.println("Error - " + e.getMessage());
					e.printStackTrace();
				} finally {
					pullCountDown.countDown(); // reduce count of CountDownLatch by 1
				}
			});
			
			/*
			 * This is to test the timeout (invisible) functionality
			 * Adds a Block between each calls - just to make sure messages got enough time to 
			 * re-appear in the queue after the timeout period
			 */
			try {
				executorService.awaitTermination(10, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e1) {
				System.err.println("Error - " + e1.getMessage());
				e1.printStackTrace();
			}
		}
	}

	/**
	 * This tests the Delete functionality 
	 */
	private void testQueueDelete(CountDownLatch pullCountDown, CountDownLatch deleteCountDown) {

		//Waits until pull functionality is completed before deleting
		try {
			pullCountDown.await(); // sub-main thread is waiting on CountDownLatch to finish
		} catch (InterruptedException ie) {
			System.err.println("Error - " + ie.getMessage());
			ie.printStackTrace();
		}
		
		System.out.println("===== starting QueueDelete test ========");

		for (int i = 0; i < DELETE_ITEMS_SIZE; i++) {

			executorService.submit(() -> {

				try {

					queueHandler.delete("test-timeout-queue");
					
				} catch (IllegalArgumentException | AssertionError e) {
					System.err.println("Error - " + e.getMessage());
					e.printStackTrace();
				} finally {
					deleteCountDown.countDown(); // reduce count of CountDownLatch by 1
				}
			});
		}
	}
	
	/*
	 * Shutdown all
	 */
	@After
	public void tearDown() {

		queueHandler.shutdown();
		executorService.shutdown();
		
		System.out.println("shutting down test services");
	}
}
