package com.example.service;

import com.example.core.MessageInfo;

/**
 * Interface for the QueueService functionality
 * This defines methods to interact with the queue service
 * 
 * @modified Rukshan Silva, 12 Jun 2016
 * @version 1.0.0
 */
public interface QueueService {

	/**
	 * Defines the push functionality for Message Queue Service
	 * 
	 * @param message - the message to be pushed to the queue
	 */
	void push(MessageInfo message);
	
	/**
	 * Defines the pull functionality for Message Queue Service
	 * 
	 * @param queueUrl - url/ name/ identifier of the queue
	 * @return message - which retrieved from the queue, or null otherwise
	 */
	MessageInfo pull(String queueUrl);
	
	/**
	 * Defines the delete functionality for Message Queue Service
	 * 
	 * @param message - which should be deleted from the queue
	 */
	void delete(String messageId);
	
	/**
	 * Shutdown all the services/ cleanup memory/  cleanup data structures
	 */
	void shutdown();
}
