package com.example.service.impl;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import com.example.core.MessageInfo;
import com.example.service.QueueService;

/**
 * InMemory Implementation class of {@link QueueService}
 * This holds the queue structure in memory
 * 
 * @modified Rukshan Silva, 12 Jun 2016
 * @version 1.0.0
 */
public class InMemoryQueueService implements QueueService {

	private static final Integer DEFAULT_EXECUTOR_POOLSIZE = 100;

	/*
	 * The ScheduledExecutor to hold a single retrieved message and add back to the queue after timeout
	 */
	private ScheduledExecutorService scheduledExecutor = null;

	/*
	 * The map which holds all the queues. A Queue is identified by queueUrl
	 * Use the thread-safe BlockingQueue
	 */
	private Map<String, BlockingQueue<MessageInfo>> queueMap = null;
	
	/*
	 * Holds the "invisible" messages - retrieved messages are kept to be added to the 
	 * 	Queue back after timeout if not deleted
	 */
	private Map<String, ScheduledFuture<?>> retrievedMessages = null;
	
	/**
	 * Default constructor - pool size of the Executor which holds the invisible jobs
	 * If not found, use the default
	 */
	public InMemoryQueueService(Integer scheduledExecutorPoolSize) {

		scheduledExecutor = Executors.newScheduledThreadPool(
				scheduledExecutorPoolSize != null ? scheduledExecutorPoolSize : DEFAULT_EXECUTOR_POOLSIZE);
		
		// Use the thread-safe ConcurrentHashMap
		queueMap = new ConcurrentHashMap<>();
		retrievedMessages = new ConcurrentHashMap<>();
	}
	
	/**
	 * Implements the InMemory push functionality for Message Queue Service
	 * 
	 * @param message - the message to be pushed to the queue
	 */
	@Override
	public void push(MessageInfo message) {
		
		BlockingQueue<MessageInfo> queue = createOrRetrieveQueue(message.getQueueUrl());
		
		try {
			queue.put(message);
			
		} catch (InterruptedException e) {
			System.err.println("exception when pushing the message [" + message.getMessageId() + "] to queue '" + message.getQueueUrl() 
				+ "' - " + e.getMessage());
			e.printStackTrace();
		}

		System.out.println("message [" + message.getMessageId() + "] added to the '" + message.getQueueUrl() + "' queue");
	}

	/**
	 * Implements the InMemory pull functionality for Message Queue Service
	 * 
	 * @param queueUrl - url/ name/ identifier of the queue
	 * @return message - which retrieved from the queue, or null otherwise
	 */
	@Override
	public MessageInfo pull(String queueUrl) {

		BlockingQueue<MessageInfo> queue = createOrRetrieveQueue(queueUrl);
		
		try {
			
			MessageInfo message = queue.take();
			
			if (message != null) {
				
				System.out.println("message [" + message.getMessageId() + "] retrieved from the '" + message.getQueueUrl() + "' queue");
				
				// 	Holds the "invisible" messages
				Runnable command = () -> { 
					queue.add(message); 
					
					System.out.println("message [" + message.getMessageId() + "] added to the '" + message.getQueueUrl() + 
							"' queue back in " + message.getMessageTimeout() + " " + message.getMessageTimeUnit().toString().toLowerCase());
				};
				
				ScheduledFuture<?> scheduledFuture = scheduledExecutor.schedule(command, message.getMessageTimeout(), message.getMessageTimeUnit());
				retrievedMessages.put(message.getMessageId(), scheduledFuture);
				
				return message;
			
			} else {
				System.err.println("no new messages found in '" + queueUrl + "' queue");
			}
		} catch (InterruptedException e) {
			System.err.println("exception when pulling the message from queue '" + queueUrl + "' - " + e.getMessage());
			e.printStackTrace();
		}
		
		return null;
	}

	/**
	 * Implements the InMemory delete functionality for Message Queue Service
	 * Removes the message and cancels the ScheduledFuture job for the given messageId.
	 * 
	 * @param message - which should be deleted from the queue
	 */
	@Override
	public void delete(String messageId) {
		
		try {
			
			ScheduledFuture<?> scheduledFuture = retrievedMessages.remove(messageId);
			
			if (scheduledFuture != null)
				scheduledFuture.cancel(true);
			
			System.out.println("message [" + messageId + "] deleted from the queue");

		} catch (Exception e) {
			System.err.println("exception when deleting the message [" + messageId + "] from queue - " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Shutdown all the services/ cleanup memory/  cleanup data structures
	 */
	@Override
	public void shutdown() {
	
		scheduledExecutor.shutdown();
		retrievedMessages.clear();
		queueMap.clear();
		
		System.out.println("shutting down the InMemory queue services");
	}
	
	/**
	 * Retrieves the queue by queueUrl. Creates a new if not found
	 */
	private BlockingQueue<MessageInfo> createOrRetrieveQueue(String queueUrl) {
		
		// Do not use external synchronization here since I use ConcurrentHashMap
		BlockingQueue<MessageInfo> queue = queueMap.get(queueUrl);
		
		if (queue == null) {
			synchronized (queueMap) {
				
				if (queue == null) {
					
					queue = new LinkedBlockingQueue<>();
					queueMap.put(queueUrl, queue);
				}
			}
		}

		return queue;
	}
}
