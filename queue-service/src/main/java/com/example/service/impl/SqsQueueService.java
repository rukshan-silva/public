package com.example.service.impl;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.UnsupportedOperationException;
import com.example.core.MessageInfo;
import com.example.service.QueueService;

public class SqsQueueService implements QueueService {
	//
	// Task 4: Optionally implement parts of me.
	//
	// This file is a placeholder for an AWS-backed implementation of
	// QueueService. It is included
	// primarily so you can quickly assess your choices for method signatures in
	// QueueService in
	// terms of how well they map to the implementation intended for a
	// production environment.
	//

	public SqsQueueService(AmazonSQSClient sqsClient) {

		throw new UnsupportedOperationException("Not implemented, yet");
	}

	@Override
	public void push(MessageInfo message) {

		throw new UnsupportedOperationException("Not implemented, yet");
	}

	@Override
	public MessageInfo pull(String queueUrl) {

		throw new UnsupportedOperationException("Not implemented, yet");
	}

	@Override
	public void delete(String messageId) {
	
		throw new UnsupportedOperationException("Not implemented, yet");
	}

	@Override
	public void shutdown() {

		throw new UnsupportedOperationException("Not implemented, yet");
	}
}
