package com.example.core;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.example.service.QueueService;

/**
 * QueueHandler is the API which handles the incoming queue requests and diverts to the
 * appropriate implementations [FileQueueService | InMemoryQueueService | SqsQueueService]
 * 
 * @author Rukshan Silva, 12 Jun 2016
 * @version 1.0.0
 */
public class QueueHandler {

	private static final Integer DEFAULT_EXECUTOR_POOLSIZE = 100;
	
	//Executor which holds pull request jobs
	private ExecutorService pullExecutorService = null;

	//Executor which holds push request jobs
	private ExecutorService pushExecutorService = null;

	//Executor which holds delete request jobs
	private ExecutorService deleteExecutorService = null;

	private QueueService queueService;

	/**
	 * Inject appropriate {@link QueueService} implementation to setter 
	 */
	public void setQueueService(QueueService queueService) {
		this.queueService = queueService;
	}

	/**
	 * Default constructor - A list of pool sizes for 3 Executors
	 * Argument 1 - pool size for pullExecutorService, which holds pull request jobs
	 * Argument 2 - pool size for pushExecutorService, which holds push request jobs
	 * Argument 3 - pool size for deleteExecutorService, which holds delete request jobs

	 * If more than 3 arguments, throw an error
	 * If not found, use the default
	 */
	public QueueHandler(Integer... executorPoolSizes) throws IllegalArgumentException {

		if (executorPoolSizes == null)
			throw new IllegalArgumentException("ExecutorPool Sizes cannot be null");

		if (executorPoolSizes.length > 3)
			throw new IllegalArgumentException("Too many arguments for the constructor."
					+ "	Usage: QueueHandler(pullExecutorPoolSize, pushExecutorPoolSize, deleteExecutorPoolSize)");
		
		pullExecutorService = Executors.newFixedThreadPool(
				executorPoolSizes.length > 0 ? executorPoolSizes[0] : DEFAULT_EXECUTOR_POOLSIZE);
		
		pushExecutorService = Executors.newFixedThreadPool(
				executorPoolSizes.length > 1 ? executorPoolSizes[1] : DEFAULT_EXECUTOR_POOLSIZE);
		
		deleteExecutorService = Executors.newFixedThreadPool(
				executorPoolSizes.length > 2 ? executorPoolSizes[2] : DEFAULT_EXECUTOR_POOLSIZE);
	}
	
	/**
	 * Handles push message requests
	 * 
	 * @param message - the message to be pushed to the queue
	 */
	public void push(MessageInfo message) throws IllegalArgumentException {
		
		if (message == null)
			throw new IllegalArgumentException("Message should not be null - push a valid message");
		
		pushExecutorService.execute(() -> { queueService.push(message); });
	}
	
	/**
	 * Handles pull message requests
	 * 
	 * @param queueUrl - url/ name/ identifier of the queue
	 * @return message - which retrieved from the queue, or null otherwise
	 */
	public MessageInfo pull(String queueUrl) throws IllegalArgumentException {

		if (queueUrl == null || queueUrl.isEmpty())
			throw new IllegalArgumentException("Queue URL should not be null - Please pull from a valid queue");

		try {
			
			Future<MessageInfo> future = (Future<MessageInfo>) pullExecutorService.submit(()-> { return queueService.pull(queueUrl); });
			return (MessageInfo) future.get();
			
		} catch (InterruptedException | ExecutionException e) {
			System.err.println("exception when pulling the message from queue '" + queueUrl + "' - " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Handles delete message requests
	 * 
	 * @param message - which should be deleted from the queue
	 */
	public void delete(String messageId) throws IllegalArgumentException {

		if (messageId == null || messageId.isEmpty())
			throw new IllegalArgumentException("Message should not be null - delete a valid message");

		deleteExecutorService.execute(() -> { queueService.delete(messageId); });
	}
	
	/**
	 * Shutdown all the services/ cleanup memory/  cleanup data structures
	 */
	public void shutdown() {
		
		queueService.shutdown();
		
		pullExecutorService.shutdown();
		pushExecutorService.shutdown();
		deleteExecutorService.shutdown();
		
		System.out.println("shutting down all the queue services");
	}
}
